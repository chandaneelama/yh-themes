$(document).ready (function(){
	$(".nav-toggler").on("click", function(){
		$('#navbarCollapse').addClass('show');
		$(this).addClass("toggle");
	});
	$(".left-cate").on("click", function(){
		$('.menu-content').toggleClass('show');
		$(this).toggleClass("change-icon");
	});
	$('body').on('click', '#navToggler', function()
	{
		$('.main-navigation').addClass('show');
		$('.body-overlay').addClass('show-overlay');  
		$('html').addClass('hidden-y') 
	});
	$('body').on('click', '.body-overlay', function()
	{   
		$(this).removeClass('show-overlay');
		$('.main-navigation').removeClass('show');
		$('html').removeClass('hidden-y'); 
		$('#navbarCollapse').removeClass('show');
		$('.nav-toggler').removeClass("toggle");  
	})
});
$('.cate-carousel').owlCarousel({
	loop:true,
	autoplay: false,
	smartSpeed: 1000,
	margin:10,
	nav:true,
	dots: false,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:4
		}
	}
});
$('.categories-carousel').owlCarousel({
	loop:true,
	autoplay: false,
	smartSpeed: 1000,
	margin:10,
	nav:true,
	dots: false,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:3
		},
		1000:{
			items:3
		}
	}
});
$('.news-carousel').owlCarousel({
	loop:true,
	autoplay: false,
	smartSpeed: 1000,
	margin:30,
	nav:false,
	dots: true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		1000:{
			items:2
		}
	}
});
$('.latest-carousel').owlCarousel({
	loop:true,
	autoplay: false,
	smartSpeed: 1000,
	margin:30,
	nav:false,
	dots: true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		1000:{
			items:3
		}
	}
});
$('.testi-carousel').owlCarousel({
	loop:true,
	autoplay: false,
	smartSpeed: 1000,
	margin:30,
	nav:false,
	dots: true,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		1000:{
			items:1
		}
	}
});
$('.product-carousel').owlCarousel({
	loop:true,
	autoplay: false,
	smartSpeed: 1000,
	margin:30,
	nav:true,
	dots: false,
	responsive:{
		0:{
			items:1
		},
		600:{
			items:1
		},
		1000:{
			items:2
		}
	}
});